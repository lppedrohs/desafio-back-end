# Desafio Back End - Blu

![login](https://i.imgur.com/TeGgYoD.png)
![transacoes](https://i.imgur.com/vjGXgen.png)

## Stack :computer:
- Ruby v2.6.6
- Rails v5.2.4.4
- Postgres
- [Docker (CE)](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Instalação do Projeto :floppy_disk:

### Primeira vez?
- Construa o projeto utilizando o Docker-Compose
```bash
docker-compose build
```

- Crie o banco de dados e carregue os dados do seed
```bash
docker-compose run --rm web rails db:create db:migrate db:seed
```

### Inicializando a aplicação :rocket:
- Você pode manter a aplicação rodando em background utilizando a opção `-d`
```bash
docker-compose up
```

### Testes :pushpin:
#### Primeira vez?
- Construa o banco de dados para o ambiente de testes
```bash
docker-compose run --rm web rails db:create db:migrate RAILS_ENV=test
```

- Utilizamos o framework de testes Rspec para execução de testes.
```bash
docker-compose run --rm web rspec RAILS_ENV=test
```

### Comandos úteis
- Acessar o console da aplicação
```bash
docker-compose run --rm web rails c
```

- Para destruir o banco e criá-lo novamente. Certifique-se de que não há nenhuma conexão acessando o banco no momento, caso contrário, receberá um erro no console.
```bash
docker-compose run --rm web rails db:drop db:create db:migrate db:seed
```

### Usuário de testes
- A aplicação utiliza autenticação via usuário com email e senha. Se você rodou o comando `db:seed` seu banco de dados já possui um usuário para acessar.
  - E-mail: user@blu.com.br
  - Senha: test123

### Futuras Melhorias :clipboard:
- Paginação na listagem de transações
- Atualização Realtime do status da importação
- Testes de integração com Capybara
- Configurar Carrierwave Uploader para o S3 em outros ambientes (staging/production)
- Utilizar o Sidekiq como Adapter de filas da aplicação para outros ambientes (staging/production)
