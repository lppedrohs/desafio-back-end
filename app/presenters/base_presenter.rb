class BasePresenter < SimpleDelegator
  attr_reader :object

  def initialize(object)
    @object = object
    __setobj__(object)
  end

  def self.wrap(collection)
    return [] unless collection.present?
    collection.map { |model| new(model) }
  end
end
