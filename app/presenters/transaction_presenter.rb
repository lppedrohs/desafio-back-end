class TransactionPresenter < BasePresenter
  include ActionView::Helpers::NumberHelper

  def amount
    number_to_currency(object.amount.to_f / 100)
  end

  def procedure_class
    object.transaction_type.input? ? 'text-success' : 'text-danger'
  end

  def datetime
    I18n.l(object.datetime, format: '%d/%m/%Y %H:%M')
  end
end
