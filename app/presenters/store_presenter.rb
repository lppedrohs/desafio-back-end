class StorePresenter < BasePresenter
  include ActionView::Helpers::NumberHelper

  def balance
    number_to_currency(object.balance.to_f / 100)
  end

  def transactions
    @transactions ||= TransactionPresenter.wrap(
      object.transactions.includes(:transaction_type).order(datetime: :desc)
    )
  end
end
