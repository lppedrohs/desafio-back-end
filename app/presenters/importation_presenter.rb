class ImportationPresenter < BasePresenter

  def long_created_at
    I18n.l(object.created_at, format: '%d/%m/%Y %H:%M')
  end

  def importation_type
    I18n.t("enum.importation.importation_type.#{object.importation_type}")
  end

  def status
    I18n.t("enum.importation.status.#{object.status}")
  end

  def meta
    JSON.pretty_generate object.meta
  end

  def self.importation_types_for_enum
    Importation.importation_types.map do |key, value|
      [I18n.t("enum.importation.importation_type.#{key}"), value]
    end
  end
end
