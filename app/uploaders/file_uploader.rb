class FileUploader < CarrierWave::Uploader::Base
  storage :file

  process :set_content_type

  def set_content_type
    model.file_content_type = file.content_type if file.content_type
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
end
