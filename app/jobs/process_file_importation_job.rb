class ProcessFileImportationJob < ApplicationJob
  attr_accessor :importation

  def perform(importation_id)
    @importation = Importation.find_by_id(importation_id)
    process_importation if @importation.present?
  end

  private

  def process_importation
    case @importation.importation_type.to_sym
    when :transactions
      ImportTransactionFileService.new(importation: @importation).call
    end
  end
end
