class Transaction < ApplicationRecord
  belongs_to :store
  belongs_to :transaction_type

  validates :store, :transaction_type, :datetime, :amount, :recipient_tax_id, presence: true

  after_create :update_store_balance

  private

  def update_store_balance
    if transaction_type.input?
      store.update(balance: store.balance + amount)
    else
      store.update(balance: store.balance - amount)
    end
  end
end
