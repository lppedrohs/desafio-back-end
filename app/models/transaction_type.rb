class TransactionType < ApplicationRecord
  enum procedure: { input: 0, output: 1 }

  validates :type_number, :description, :procedure, :signal, presence: true
end
