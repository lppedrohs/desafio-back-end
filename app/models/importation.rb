class Importation < ApplicationRecord
  enum importation_type: { transactions: 'transactions' }
  enum status: {
    waiting: 'waiting', in_progress: 'in_progress', completed: 'completed', error: 'error'
  }, _prefix: true

  validates :file, :importation_type, :status, presence: true

  mount_uploader :file, FileUploader
end
