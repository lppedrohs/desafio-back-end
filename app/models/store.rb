class Store < ApplicationRecord
  has_many :transactions

  validates :owner, :name, :balance, presence: true

  before_save :set_slug, if: :name_changed?

  private

  def set_slug
    self.slug = name.parameterize
  end
end
