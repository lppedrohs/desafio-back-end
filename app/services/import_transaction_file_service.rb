class ImportTransactionFileService
  class ProcessError < StandardError; end

  attr_accessor :importation, :file, :created_transactions

  FILE_EXTENSION = 'text/plain'.freeze

  def initialize(importation:)
    @importation = importation
    @file        = importation.file

    @created_transactions = []
  end

  def call
    check_file

    process_file
    complete_process
  rescue ProcessError => e
    importation.status = :error
    importation.meta.merge!(error: e.message)
    importation.save
  ensure
    return importation
  end

  private

  def process_file
    ActiveRecord::Base.transaction do
      file_content.each_with_index do |row, index|
        transaction = TransactionFileParser.transaction_from_row(row, index)

        if transaction.errors.blank? && transaction.save
          created_transactions << transaction.id
        else
          raise_error(custom_message: transaction.errors.full_messages.to_sentence)
        end
      end
    end
  end

  def complete_process
    importation.status = :completed
    importation.meta.merge!(created_transactions: created_transactions)
    importation.save
  end

  def check_file
    raise_error(:content_type) if importation.file_content_type != FILE_EXTENSION
    raise_error(:blank_file) if file_content.blank?
  end

  def raise_error(message_key = nil, custom_message: nil)
    raise ProcessError.new(custom_message) if custom_message.present?
    
    message = I18n.t("services.import_transaction_file.errors.#{message_key}")
    raise ProcessError.new(message)
  end

  def file_content
    @file_content ||= begin
      return [] if file.blank?
      file.read.force_encoding('UTF-8').split("\n")
    end
  end
end
