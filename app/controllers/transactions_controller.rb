class TransactionsController < ApplicationController
  before_action :index, :set_stores

  def index
  end

  private

  def set_stores
    @stores = Store.all.collect { |s| [s.name, s.id] } || []
    @stores.prepend([t('.select_store.default'), 0])

    store = Store.find_by_id(params[:store_id] || session[:store_id])
    session[:store_id] = store.id if store.present?
    @current_store = StorePresenter.new(store)
  end
end
