class ImportationsController < ApplicationController

  def index
    @importation = Importation.new
    @importations = ImportationPresenter.wrap(Importation.order(created_at: :desc).last(10))
  end

  def create
    importation = Importation.new(importation_params)
    importation.status = :in_progress

    if importation.save
      ProcessFileImportationJob.perform_later(importation.id)
      flash[:info] = t('.flash.success')
    else
      flash[:warning] = importation.errors.full_messages.to_sentence
    end

    redirect_to importations_path
  end

  private

  def importation_params
    params.require(:importation).permit(:file, :importation_type)
  end
end
