# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_21_014511) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "importations", force: :cascade do |t|
    t.string "importation_type", null: false
    t.string "file"
    t.string "file_content_type"
    t.json "meta", default: {}
    t.string "status", default: "waiting"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stores", force: :cascade do |t|
    t.string "owner"
    t.string "name", null: false
    t.string "slug", null: false
    t.bigint "balance", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_stores_on_slug"
  end

  create_table "transaction_types", force: :cascade do |t|
    t.integer "type_number", null: false
    t.string "description"
    t.integer "procedure", null: false
    t.string "signal", limit: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["procedure"], name: "index_transaction_types_on_procedure"
    t.index ["type_number"], name: "index_transaction_types_on_type_number"
  end

  create_table "transactions", force: :cascade do |t|
    t.bigint "store_id"
    t.bigint "transaction_type_id"
    t.datetime "datetime"
    t.bigint "amount", null: false
    t.string "recipient_tax_id", null: false
    t.string "card_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_transactions_on_store_id"
    t.index ["transaction_type_id"], name: "index_transactions_on_transaction_type_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "transactions", "stores"
  add_foreign_key "transactions", "transaction_types"
end
