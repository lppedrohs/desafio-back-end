class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.belongs_to :store, foreign_key: true
      t.belongs_to :transaction_type, foreign_key: true
      
      t.datetime :datetime
      t.bigint :amount, null: false
      t.string :recipient_tax_id, null: false
      t.string :card_number

      t.timestamps
    end
  end
end
