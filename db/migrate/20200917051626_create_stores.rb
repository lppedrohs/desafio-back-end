class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :owner
      t.string :name, null: false
      t.string :slug, index: true, null: false
      t.bigint :balance, default: 0, null: false

      t.timestamps
    end
  end
end
