class CreateImportations < ActiveRecord::Migration[5.2]
  def change
    create_table :importations do |t|
      t.string :importation_type, null: false
      t.string :file
      t.string :file_content_type
      t.json :meta, default: {}
      t.string :status, default: :waiting

      t.timestamps
    end
  end
end
