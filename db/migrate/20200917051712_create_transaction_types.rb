class CreateTransactionTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :transaction_types do |t|
      t.integer :type_number, index: true, null: false
      t.string :description
      t.integer :procedure, index: true, null: false
      t.string :signal, limit: 1, null: false

      t.timestamps
    end
  end
end
