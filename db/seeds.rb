TransactionType.create([
  { type_number: 1, description: 'Débito', procedure: :input, signal: '+' },
  { type_number: 2, description: 'Boleto', procedure: :output, signal: '-' },
  { type_number: 3, description: 'Financiamento', procedure: :output, signal: '-' },
  { type_number: 4, description: 'Crédito', procedure: :input, signal: '+' },
  { type_number: 5, description: 'Recebimento Empréstimo', procedure: :input, signal: '+' },
  { type_number: 6, description: 'Vendas', procedure: :input, signal: '+' },
  { type_number: 7, description: 'Recebimento TED', procedure: :input, signal: '+' },
  { type_number: 8, description: 'Recebimento DOC', procedure: :input, signal: '+' },
  { type_number: 9, description: 'Aluguel', procedure: :output, signal: '-' }
])

User.create(email: 'user@blu.com.br', password: 'test123')