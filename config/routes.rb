Rails.application.routes.draw do
  devise_for :users

  root 'transactions#index'

  resources :transactions, only: :index
  resources :importations, only: %i[index create]
end
