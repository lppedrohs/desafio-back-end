module TransactionFileParser
  extend self

  TRANSACTION_TYPE_NUMBER_RANGE = (0..0).freeze
  DATE_RANGE                    = (1..8).freeze
  AMOUNT_RANGE                  = (9..18).freeze
  RECIPIENT_TAX_ID_RANGE        = (19..29).freeze
  CARD_NUMBER_RANGE             = (30..41).freeze
  TIME_RANGE                    = (42..47).freeze
  STORE_OWNER_RANGE             = (48..61).freeze
  STORE_NAME_RANGE              = (62..79).freeze

  def transaction_from_row(row, line = 0)
    transaction = Transaction.new

    if row.length != 80
      add_error(transaction, :row, line)
      return transaction
    end

    transaction.store            = find_store(transaction, row, line)
    transaction.transaction_type = find_transaction_type(transaction, row, line)

    transaction.amount           = parse_amount(transaction, row, line)
    transaction.datetime         = parse_datetime(transaction, row, line)
    transaction.card_number      = parse_card_number(transaction, row, line)
    transaction.recipient_tax_id = parse_recipient_tax_id(transaction, row, line)

    transaction
  end

  def find_store(transaction, row, line)
    store_owner, store_name = row[STORE_OWNER_RANGE], row[STORE_NAME_RANGE]

    if store_owner.blank? || store_name.blank? ||
        store_name.length != STORE_NAME_RANGE.size || store_owner.length != STORE_OWNER_RANGE.size

      add_error(transaction, :store, line)
      return
    end

    store = Store.lock.find_by(
      'lower(owner) = ? AND slug = ?',
      store_owner.squish.downcase,
      store_name.parameterize
    )

    return store if store.present?

    Store.create(owner: store_owner.squish.titlecase, name: store_name.squish.titlecase)
  end

  def find_transaction_type(transaction, row, line)
    transaction_type_number = row[TRANSACTION_TYPE_NUMBER_RANGE]
    transaction_type = TransactionType.find_by_type_number(transaction_type_number)

    return transaction_type if transaction_type.present?

    add_error(transaction, :transaction_type, line)
  end

  def parse_datetime(transaction, row, line)
    date, time = row[DATE_RANGE], row[TIME_RANGE]

    if date.scan(/\D/).empty? && date.length == DATE_RANGE.size && 
        time.scan(/\D/).empty? && time.length == TIME_RANGE.size

      datetime = DateTime.parse("#{date} #{time} -0300") rescue nil

      return datetime if datetime.present?
    end

    add_error(transaction, :datetime, line)
  end

  def parse_amount(transaction, row, line)
    amount = row[AMOUNT_RANGE]

    return amount if amount&.length == AMOUNT_RANGE.size && amount.to_i.positive?

    add_error(transaction, :amount, line)
  end

  def parse_card_number(transaction, row, line)
    card_number = row[CARD_NUMBER_RANGE]

    return card_number if card_number&.length == CARD_NUMBER_RANGE.size

    add_error(transaction, :card_number, line)
  end

  def parse_recipient_tax_id(transaction, row, line)
    recipient_tax_id = row[RECIPIENT_TAX_ID_RANGE]

    return recipient_tax_id if recipient_tax_id&.length == RECIPIENT_TAX_ID_RANGE.size

    add_error(transaction, :recipient_tax_id, line)
  end

  def add_error(transaction, attribute, line)
    transaction.errors.add(:base, "#{attribute}_invalid_parsing".to_sym, line: line)
  end
end
