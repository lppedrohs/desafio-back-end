FactoryBot.define do
  factory :transaction_type do
    type_number { rand(0..9) }
    description { Faker::Lorem.sentence(word_count: 3) }
    procedure   { TransactionType.procedures.values.sample }
    signal      { ['+', '-'].sample }
  end
end
