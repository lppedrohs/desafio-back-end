FactoryBot.define do
  factory :store do
    owner   { Faker::Name.name }
    name    { Faker::Company.name }
    balance { Faker::Number.number(digits: 5) }
  end
end
