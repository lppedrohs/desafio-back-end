FactoryBot.define do
  factory :transaction do
    association :store
    association :transaction_type

    datetime         { Faker::Time.backward }
    card_number      { Faker::Number.number(digits: 12) }
    recipient_tax_id { Faker::IDNumber.brazilian_citizen_number }
    amount           { Faker::Number.number(digits: 5) }
  end
end
