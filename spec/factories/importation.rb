FactoryBot.define do
  factory :importation do
    status           { :waiting }
    importation_type { :transactions }
    file { File.open('spec/support/example_importation_file.txt') }
    file_content_type { 'text/plain' }
  end
end
