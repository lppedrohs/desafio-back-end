require 'rails_helper'

RSpec.describe TransactionFileParser do
  describe '#transaction_from_row' do
    before { create(:transaction_type, type_number: 2) }

    context 'when is a valid row' do
      let(:row) { '2201903010000010700845152540738723****9987123333MARCOS PEREIRAMERCADO DA AVENIDA' }
      let(:line) { rand(1..100) }

      it 'should return the correct store' do
        store_name = Faker::Lorem.characters(number: TransactionFileParser::STORE_NAME_RANGE.size).titlecase
        store_owner = Faker::Lorem.characters(number: TransactionFileParser::STORE_OWNER_RANGE.size).titlecase
        row[TransactionFileParser::STORE_NAME_RANGE] = store_name
        row[TransactionFileParser::STORE_OWNER_RANGE] = store_owner

        transaction = TransactionFileParser.transaction_from_row(row, line)

        expect(transaction.errors.blank?).to be_truthy

        expect(transaction.store.name).to eql(store_name)
        expect(transaction.store.owner).to eql(store_owner)
      end

      it 'should return the correct transaction_type' do
        transaction_type = create(:transaction_type)
        row[0] = transaction_type.type_number.to_s

        transaction = TransactionFileParser.transaction_from_row(row, line)

        expect(transaction.errors.blank?).to be_truthy

        expect(transaction.transaction_type.type_number).to eql(transaction_type.type_number)
      end

      it 'should return the correct datetime' do
        datetime = Faker::Time.backward
        row[TransactionFileParser::DATE_RANGE] = datetime.strftime('%Y%m%d')
        row[TransactionFileParser::TIME_RANGE] = datetime.strftime('%H%M%S')

        transaction = TransactionFileParser.transaction_from_row(row, line)

        expect(transaction.errors.blank?).to be_truthy

        expect(transaction.datetime.to_s).to eql("#{datetime.strftime("%Y-%m-%d %H:%M:%S")} -0300")
      end

      it 'should return the correct amount' do
        amount = Faker::Number.number(digits: 5).to_s.rjust(TransactionFileParser::AMOUNT_RANGE.size, '0')
        row[TransactionFileParser::AMOUNT_RANGE] = amount

        transaction = TransactionFileParser.transaction_from_row(row, line)

        expect(transaction.errors.blank?).to be_truthy

        expect(transaction.amount).to eql(amount.to_i)
      end

      it 'should return the correct card number' do
        card_number = Faker::Number.number(digits: 12).to_s
        row[TransactionFileParser::CARD_NUMBER_RANGE] = card_number

        transaction = TransactionFileParser.transaction_from_row(row, line)

        expect(transaction.errors.blank?).to be_truthy

        expect(transaction.card_number).to eql(card_number)
      end

      it 'should return the correct card number' do
        recipient_tax_id = Faker::IDNumber.brazilian_citizen_number
        row[TransactionFileParser::RECIPIENT_TAX_ID_RANGE] = recipient_tax_id

        transaction = TransactionFileParser.transaction_from_row(row, line)

        expect(transaction.errors.blank?).to be_truthy

        expect(transaction.recipient_tax_id).to eql(recipient_tax_id)
      end
    end

    context 'when is a invalid row' do
      let(:row) { '2201903010000010700845152540738723' }
      let(:line) { rand(1..100) }

      it 'should return row_invalid_parsing error' do
        transaction = TransactionFileParser.transaction_from_row(row, line)

        expect(transaction.errors.any?).to be_truthy
        expect(
          transaction.errors.details[:base].any? { |detail| detail[:error] == :row_invalid_parsing }
        ).to be_truthy
      end
    end
  end
end
