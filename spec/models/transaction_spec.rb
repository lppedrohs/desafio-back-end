RSpec.describe Transaction, type: :model do
  describe '#callbacks' do
    describe '#update_store_balance' do
      context 'when a new transaction is created' do
        let(:store) { create(:store) }

        let(:input_transaction_type) { create(:transaction_type, procedure: :input) }
        let(:output_transaction_type) { create(:transaction_type, procedure: :output) }

        it 'should update store balance if transaction type is input' do
          current_balance = store.balance

          transaction = create(:transaction, store: store, transaction_type: input_transaction_type)

          expect(store.reload.balance).to eql(current_balance + transaction.amount)
        end

        it 'should update store balance if transaction type is output' do
          current_balance = store.balance

          transaction = create(:transaction, store: store, transaction_type: output_transaction_type)

          expect(store.reload.balance).to eql(current_balance - transaction.amount)
        end
      end
    end
  end
end
