RSpec.describe ImportTransactionFileService do
  describe '#call' do
    # Present in default file of importation factory
    before { create(:transaction_type, type_number: 3) }

    context 'when importation file is valid' do
      it 'should create the transactions successfully' do
        importation = create(:importation, importation_type: :transactions)

        importation = ImportTransactionFileService.new(importation: importation).call

        expect(importation.status).to eql('completed')
        expect(importation.meta['error']).to be_blank
        expect(importation.meta['created_transactions'].any?).to be_truthy
        expect(
          importation.meta['created_transactions'].all? { |t| Transaction.exists?(id: t) }
        ).to be_truthy
      end
    end

    context 'when importation file has invalid content_type' do
      it 'should return an error' do
        importation = create(:importation, file_content_type: 'image/jpg')

        importation = ImportTransactionFileService.new(importation: importation).call

        expect(importation.status).to eql('error')
        expect(importation.meta['error'].present?).to be_truthy
        expect(importation.meta['created_transactions'].blank?).to be_truthy
      end
    end

    context 'when importation file is empty' do
      it 'should return an error' do
        importation = create(:importation, file: File.open('tmp/empty.txt', 'w'))

        importation = ImportTransactionFileService.new(importation: importation).call

        expect(importation.status).to eql('error')
        expect(importation.meta['error'].present?).to be_truthy
        expect(importation.meta['created_transactions'].blank?).to be_truthy
      ensure
        File.delete('tmp/empty.txt')
      end
    end
  end
end
